
package main


import ( 
		"time"
		"net/http"
		"fmt"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.127:8092",
			IdleTimeout: duration,
		//	Handler    : 
		}

	http.HandleFunc("/", metodoEUrl)
	http.HandleFunc("/imagem", imagens)
	http.Handle("/imagens/", http.StripPrefix("/imagens/", http.FileServer(http.Dir("./imagem"))))
	http.HandleFunc("/hello", hello)

	server.ListenAndServe()
}

func imagens(w http.ResponseWriter, r *http.Request) {
	
	fmt.Fprintf(w, "<h1>Pagani Cinque</h1>")
    fmt.Fprintf(w, "<img src='imagens/pagani.jpg' alt='imagens' style='width:1280px;height:720px;'>")
}

func metodoEUrl(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, r.Method + " " + r.URL.Path + "?" + r.URL.RawQuery + r.URL.Fragment)
	return 
}

func hello(w http.ResponseWriter, r *http.Request){
	w.Write([]byte("Hello World"))
	return
}