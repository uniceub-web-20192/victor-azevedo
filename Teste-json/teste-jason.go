package main

import ( "net/http"
		 "time" 
		 "log"
		 "io/ioutil"
		 "encoding/json"
		 "github.com/gorilla/mux")

type User struct {

	Name   string `json:name`
	Email  string `json:email`
	Pass   string `json:pass` 
	Gender string `json:gender`
}


func main() {

	StartServer()
	
	// Essa linha deve ser executada sem alteração
	// da função StartServer	
	log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/signup", Signup).Methods("POST").HeadersRegexp("Content-Type", "application/json")

	server := &http.Server{
			Addr       : "172.22.51.133:8092",
			IdleTimeout: duration,
			Handler    : r, 
	}

	log.Print(server.ListenAndServe())
}

func Signup(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)
	
	json.Unmarshal(body, &u)
	userjson,_ := json.Marshal(&u)
	res.Write([]byte(userjson))


}

