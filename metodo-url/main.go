//Help source: https://stackoverflow.com/questions/47115482/how-do-you-insert-an-image-into-a-html-golang-file
package main


import ( 
		"time"
		"net/http"
		"fmt"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.127:8092",
			IdleTimeout: duration,
		//	Handler    : 
		}

	http.HandleFunc("/", metodoEUrl)

	http.HandleFunc("/hello", hello)

	server.ListenAndServe()
}

func metodoEUrl(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, r.Method + " " + r.URL.String())
	return 

}
